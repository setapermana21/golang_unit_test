package helper

import (
	"fmt"
	"runtime"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func BenchmarkTable(b *testing.B)  {
	benchmarks := []struct {
		name string
		request string
	}{
		{
			name: "Seta",
			request: "Seta",
		},
		{
		name: "Mifta",
		request: "Mifta",
		},
		{
		name: "SetaPermana",
		request: "Seta Permana",
		},
		{
		name: "Mifta",
		request: "Miftahul Hasanah",
		},
	}
	for _, benbenchmarks := range benchmarks {
		b.Run(benbenchmarks.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				Hello(benbenchmarks.request)
			}
		})
	}
}

func BenchmarkSub(b *testing.B)  {
	b.Run("Seta", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			Hello("Seta")
		}
	})
	b.Run("Mifta", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			Hello("Mifta")
		}
	})
}

func BenchmarkHelloSeta(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Hello("Seta")
	}
}

func BenchmarkHelloMifta(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Hello("Mifta")
	}
}

func TestMain(m *testing.M) {
	// before
	fmt.Println("Sebelum TestMain")

	m.Run()

	// after
	fmt.Println("Sesudah TestMain")
}

func TestTableHello(t *testing.T) {
	tests := []struct {
		name     string
		request  string
		expected string
	}{
		{
			name:     "Seta",
			request:  "Seta",
			expected: "Hello Seta",
		},
		{
			name:     "Mifta",
			request:  "Mifta",
			expected: "Hello Mifta",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := Hello(test.request)
			require.Equal(t, test.expected, result)
		})
	}
}

func TestSubTest(t *testing.T) {
	t.Run("Seta", func(t *testing.T) {
		result := Hello("Seta")
		require.Equal(t, "Hello Seta", result, "result harusnya Hello Seta")
	})
	t.Run("Mifta", func(t *testing.T) {
		result := Hello("Mifta")
		require.Equal(t, "Hello Mifta", result, "result harusnya 'Hello Mifta'")
	})
}

func TestHelloRequire(t *testing.T) {
	result := Hello("Mifta")
	require.Equal(t, "Hello Mifta", result, "result harusnya 'Hello Mifta'")
	fmt.Println("TestHelloMifta dengan Assert selesai")
}

func TestHelloAssertion(t *testing.T) {
	result := Hello("Mifta")
	assert.Equal(t, "Hello Mifta", result, "result harusnya 'Hello Mifta'")
	fmt.Println("TestHelloMifta dengan Assert selesai")
}

func TestSkip(t *testing.T) {
	if runtime.GOOS == "windows" {
		t.Skip("Unit test tidak dijalankan di OS Windows")
	}
	fmt.Println("TestSkip selesai")
}

func TestHelloMifta(t *testing.T) {
	result := Hello("Mifta")

	if result != "Hello Mifta" {
		// panic, tidak disarankan
		// panic("Output bukan 'Hello Mifta'")

		// Fail, error tetap melanjutkan kode program selanjtunya
		// t.Fail()

		// Error, disarankan digunakan
		t.Error("result harusnya 'Hello Mifta'")
	}
	fmt.Println("TestHelloMifta selesai")
}

func TestHelloSeta(t *testing.T) {
	result := Hello("Seta")

	if result != "Hello Seta" {
		// panic, tidak disarankan
		// panic("Output bukan 'Hello Seta'")

		// FailNow, error tidak melanjutkan kode program selanjtunya
		// t.FailNow()

		// Fatal, disarankan digunakan
		t.Fatal("result harusnya 'Hello Seta'")
	}
	fmt.Println("TestHelloSeta selesai")
}
